import {Component, OnInit} from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

import {WeatherService} from '../../services/weather/weather.service';
import {FavoriteService} from '../../services/favorite/favorite.service';

@Component({
  selector: 'app-favorite-list',
  templateUrl: './favorite-list.component.html',
  styleUrls: ['./favorite-list.component.css']
})
export class FavoriteListComponent implements OnInit {
  favorite = [];

  constructor(
    private weatherService: WeatherService,
    private favoriteService: FavoriteService
  ) {
  }

  ngOnInit() {
    this.loadFavoriteData();
  }

  loadFavoriteData() {
    this.favorite = this.favoriteService.getFavorite();

    const favoriteWeatherPromises = [];
    this.favorite.forEach(({id}) => {
      favoriteWeatherPromises.push(this.weatherService.getWeather(id));
    });

    Promise.all(favoriteWeatherPromises).then((data) => {
      this.favorite = data;
    });
  }

  onRemoveElement({id}) {
    this.favorite = this.favorite.filter((city) => city.id !== id);
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.favorite, event.previousIndex, event.currentIndex);
    this.favoriteService.updateFavoriteList(this.favorite);
  }

}
