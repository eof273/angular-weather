import {Component, OnInit} from '@angular/core';

import {WeatherService} from '../../services/weather/weather.service';
import {HistoryService} from '../../services/history/history.service';
import {FavoriteService} from '../../services/favorite/favorite.service';

@Component({
  selector: 'app-weather-list',
  templateUrl: './weather-list.component.html',
  styleUrls: ['./weather-list.component.css']
})
export class WeatherListComponent implements OnInit {
  history = [];

  constructor(
    private weatherService: WeatherService,
    private historyService: HistoryService,
    private favoriteService: FavoriteService,
  ) {
  }

  ngOnInit() {
    this.loadHistoryData();
  }

  loadHistoryData() {
    this.history = this.historyService.getHistory();

    const historyWeatherPromises = [];
    this.history.forEach(({id}) => {
      historyWeatherPromises.push(this.weatherService.getWeather(id));
    });

    Promise.all(historyWeatherPromises).then((data) => {
      this.history = data;
    });
  }

  onRemoveElement({id}) {
    this.history = this.history.filter((city) => city.id !== id);
  }

  isFavorite({id}) {
    return this.favoriteService.isFavorite(id);
  }

}
