import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';

import {WeatherService} from '../../services/weather/weather.service';
import {HistoryService} from '../../services/history/history.service';
import {FavoriteService} from '../../services/favorite/favorite.service';

import {Weather} from '../../models/weather';

import {TELEGRAM_SHARE_LINK, ERROR_URL} from '../../constants';

@Component({
  selector: 'app-city-info',
  templateUrl: './city-info.component.html',
  styleUrls: ['./city-info.component.css']
})
export class CityInfoComponent implements OnInit {
  id;
  city;
  presentWeather: Weather;
  futureWeather: Weather[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private weatherService: WeatherService,
    private historyService: HistoryService,
    private favoriteService: FavoriteService
  ) {
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((params) => {
      this.id = +params.get('cityId');

      this.weatherService
        .getWeather(this.id)
        .then(({city, presentWeather, futureWeather}) => {
          this.city = city;
          this.presentWeather = presentWeather;
          this.futureWeather = futureWeather;
        })
        .then(() => {
          this.historyService.addToHistory({
            id: this.id,
            city: this.city
          });
        })
        .catch(() => {
          this.router.navigate([`/${ERROR_URL}`]);
        });
    });
  }

  addToFavorite() {
    this.favoriteService.addToFavorite({
      id: this.id,
      city: this.city
    });
  }

  removeFromFavorite() {
    this.favoriteService.removeFromFavorite(this.id);
  }

  isFavorite() {
    return this.favoriteService.isFavorite(this.id);
  }

  getTLink() {
    return `${TELEGRAM_SHARE_LINK}?url=${window.location}&text=${`${this.city} weather;`}`;
  }

}
