import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {DragDropModule} from '@angular/cdk/drag-drop';

import {AutocompleteLibModule} from 'angular-ng-autocomplete';

import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';

import {WeatherListComponent} from './pages/weather-list/weather-list.component';
import {CityInfoComponent} from './pages/city-info/city-info.component';
import {FavoriteListComponent} from './pages/favorite-list/favorite-list.component';
import {ErrorComponent} from './pages/error/error.component';

import {MainMenuComponent} from './components/main-menu/main-menu.component';
import {CityCardComponent} from './components/city-card/city-card.component';
import {SearchFieldComponent} from './components/search-field/search-field.component';
import {WeatherCardComponent} from './components/weather-card/weather-card.component';

@NgModule({
  declarations: [
    AppComponent,

    WeatherListComponent,
    CityInfoComponent,
    FavoriteListComponent,
    ErrorComponent,

    MainMenuComponent,
    CityCardComponent,
    SearchFieldComponent,
    WeatherCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AutocompleteLibModule,
    DragDropModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
