export const WEATHER_API_KEY = 'ab0226694357167784e1da0854f64987';
export const WEATHER_API_URL = 'https://api.openweathermap.org/data/2.5/forecast';
export const FAVORITE_URL = 'my';
export const ERROR_URL = 'oh-no';
export const TELEGRAM_SHARE_LINK = 'https://telegram.me/share/url';
export const SEARCH_CITY_URL = 'https://immense-caverns-46307.herokuapp.com/cities?query=';
