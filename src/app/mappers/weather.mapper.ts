import {Weather} from '../models/weather';

export class WeatherMapper {
  static toClient(data) {
    const id = data.city.id;
    const city = data.city.name;
    const [presentWeather, ...futureWeather] = WeatherMapper.listMapper(data.list);

    return {
      id,
      city,
      presentWeather,
      futureWeather
    };
  }

  static listMapper(list): Weather[] {
    const data = [];

    for (let i = 0, l = list.last; i < l || data.length < 5; i = i + 8) {
      const item = list[i];

      data.push({
        temp: `${(+item.main.temp - 273).toFixed(2)} &#8451;`,
        pressure: `${item.main.pressure} hPa`,
        humidity: `${item.main.humidity}%`,
        icon: `https://openweathermap.org/img/wn/${item.weather[0].icon}${i ? '' : '@2x'}.png`,
        description: item.weather[0].description,
        windSpeed: `${item.wind.speed} meter/sec`,
        date: new Date(item.dt_txt)
      });
    }

    return data;
  }

}
