import {Component, EventEmitter, Output} from '@angular/core';
import {Router} from '@angular/router';

import {CityService} from '../../services/city/city.service';
import {WeatherService} from '../../services/weather/weather.service';

import {City} from '../../models/city';

@Component({
  selector: 'app-search-field',
  templateUrl: './search-field.component.html',
  styleUrls: ['./search-field.component.css']
})
export class SearchFieldComponent {
  @Output() lostFocus: EventEmitter<any> = new EventEmitter();

  data: City[] = [];
  keyword = 'name';
  isLoading = false;

  constructor(
    private cityService: CityService,
    private weatherService: WeatherService,
    private router: Router
  ) {
  }

  onClosed() {
    this.lostFocus.emit();
  }

  selectEvent({id}) {
    this.router.navigate(['/city-info', id]);
  }

  onChangeSearch(val: string) {
    this.isLoading = true;

    this.cityService
      .searchCity(val)
      .subscribe((data: City[]) => {
        this.isLoading = false;
        this.data = data;
      });
  }

}
