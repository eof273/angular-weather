import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

import {HistoryService} from '../../services/history/history.service';
import {FavoriteService} from '../../services/favorite/favorite.service';

import {Weather} from '../../models/weather';

@Component({
  selector: 'app-city-card',
  templateUrl: './city-card.component.html',
  styleUrls: ['./city-card.component.css']
})
export class CityCardComponent implements OnInit {
  @Input() weatherData;
  @Input() favorite;
  @Input() fromFavorite;
  @Output() removeFromHistoryEvent: EventEmitter<any> = new EventEmitter();
  @Output() removeFromFavoriteEvent: EventEmitter<any> = new EventEmitter();

  id;
  city;
  weather: Weather;

  constructor(
    private historyService: HistoryService,
    private favoriteService: FavoriteService
  ) {
  }

  ngOnInit() {
    const {id, city, presentWeather} = this.weatherData;

    this.id = id;
    this.city = city;
    this.weather = presentWeather;
  }

  addToFavorite() {
    this.favorite = true;

    this.favoriteService.addToFavorite({
      id: this.id,
      city: this.city
    });
  }

  removeFromFavorite() {
    this.favorite = false;

    this.favoriteService.removeFromFavorite(this.id);
    this.removeFromFavoriteEvent.emit({id: this.id});
  }

  remove() {
    this.historyService.removeFromHistory(this.id);

    if (this.favorite && this.fromFavorite) {
      this.removeFromFavorite();
    }

    this.removeFromHistoryEvent.emit({id: this.id});
  }

}
