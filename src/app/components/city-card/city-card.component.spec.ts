import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterModule} from '@angular/router';

import {CityCardComponent} from './city-card.component';

import {Weather} from '../../models/weather';

describe('CityCardComponent', () => {
  let component: CityCardComponent;
  let fixture: ComponentFixture<CityCardComponent>;

  const mockWeather: Weather = {
    icon: '',
    date: new Date(),
    description: 'something bad',
    humidity: '9999',
    pressure: '8888',
    temp: '7777',
    windSpeed: '6666'
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterModule],
      declarations: [CityCardComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityCardComponent);
    component = fixture.componentInstance;
    component.weatherData = mockWeather;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
