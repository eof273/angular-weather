import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {WeatherMapper} from '../../mappers/weather.mapper';

import {WEATHER_API_KEY, WEATHER_API_URL} from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  getWeather(cityId: number) {
    return this.httpClient
      .get(`${WEATHER_API_URL}?id=${cityId}&APPID=${WEATHER_API_KEY}`)
      .toPromise()
      .then(WeatherMapper.toClient);
  }

}
