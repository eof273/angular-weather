import {Injectable} from '@angular/core';

import {StorageService} from '../storage/storage.service';

@Injectable({
  providedIn: 'root'
})
export class FavoriteService {
  private favorite = [];

  constructor() {
    this.favorite = StorageService.getFromLocalStorage('favorite', []);
  }

  addToFavorite(city) {
    this.favorite = this.favorite.filter(({id}) => city.id !== id);
    this.favorite.push(city);
    this.saveToLocalStorage();
  }

  removeFromFavorite(id) {
    this.favorite = this.favorite.filter((city) => city.id !== id);
    this.saveToLocalStorage();
  }

  getFavorite() {
    return this.favorite;
  }

  updateFavoriteList(list) {
    this.favorite = list;
    this.saveToLocalStorage();
  }

  isFavorite(id) {
    return this.favorite.some((city) => city.id === id);
  }

  private saveToLocalStorage() {
    StorageService.saveToLocalStorage('favorite', this.favorite);
  }

}
