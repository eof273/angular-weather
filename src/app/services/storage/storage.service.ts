import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  static getFromLocalStorage(key, defaultValue) {
    return JSON.parse(localStorage.getItem(key)) || defaultValue;
  }

  static saveToLocalStorage(key, data) {
    localStorage.setItem(key, JSON.stringify(data));
  }

}
