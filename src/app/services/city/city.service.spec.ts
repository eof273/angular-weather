import {TestBed} from '@angular/core/testing';

import {HttpClientModule} from '@angular/common/http';

import {CityService} from './city.service';

describe('CityService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule]
  }));

  it('should be created', () => {
    const service: CityService = TestBed.get(CityService);
    expect(service).toBeTruthy();
  });
});
