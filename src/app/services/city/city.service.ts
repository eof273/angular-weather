import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {SEARCH_CITY_URL} from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  searchCity(query) {
    return this.httpClient
      .get(SEARCH_CITY_URL + query);
  }

}
