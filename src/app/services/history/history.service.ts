import {Injectable} from '@angular/core';

import {StorageService} from '../storage/storage.service';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {
  private history = [];

  constructor() {
    this.history = StorageService.getFromLocalStorage('history', []);
  }

  addToHistory(city) {
    this.history = this.history.filter(({id}) => city.id !== id);
    this.history.push(city);
    this.saveToLocalStorage();
  }

  removeFromHistory(id) {
    this.history = this.history.filter((city) => city.id !== id);
    this.saveToLocalStorage();
  }

  getHistory() {
    return this.history;
  }

  private saveToLocalStorage() {
    StorageService.saveToLocalStorage('history', this.history);
  }

}
