import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {WeatherListComponent} from './pages/weather-list/weather-list.component';
import {CityInfoComponent} from './pages/city-info/city-info.component';
import {FavoriteListComponent} from './pages/favorite-list/favorite-list.component';
import {ErrorComponent} from './pages/error/error.component';

import {FAVORITE_URL, ERROR_URL} from './constants';

const routes: Routes = [
  {path: '', component: WeatherListComponent},
  {path: 'city-info/:cityId', component: CityInfoComponent},
  {path: FAVORITE_URL, component: FavoriteListComponent},
  {path: ERROR_URL, component: ErrorComponent},
  {path: '**', redirectTo: ERROR_URL}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
