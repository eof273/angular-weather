export class City {
  '_id': string;
  id: number;
  name: string;
  country: string;
}
