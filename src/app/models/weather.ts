export class Weather {
  temp: string;
  pressure: string;
  humidity: string;
  icon: string;
  description: string;
  windSpeed: string;
  date: object;
}
