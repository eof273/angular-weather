import {TestBed, async} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';

import {AutocompleteLibModule} from 'angular-ng-autocomplete';

import {AppComponent} from './app.component';
import {MainMenuComponent} from './components/main-menu/main-menu.component';
import {SearchFieldComponent} from './components/search-field/search-field.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AutocompleteLibModule,
        HttpClientModule,
        RouterTestingModule
      ],
      declarations: [
        AppComponent,
        MainMenuComponent,
        SearchFieldComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
