# AngularWeather

Тестовое задание, использующее api [openweathermap.org](https://openweathermap.org/api);

Т.к. `openweathermap` не предоставляет возможности нормального поиска по городам, пришлось развернуть [дополнительный api](https://github.com/eof273/city-api), реализующий поиск по базе городов `openweathermap`, представляющий собой просдойку между `AngularWeather` и базой на `mlab`. Из-за этого поиск по городам работает медлено;

## Запуск

Устанавливаем зависимости:

```
npm i
```

Запускаем dev сервер:

```
ng serve
``` 

или через команду `serve` в скриптах `package.json`  
Развернётся на `http://localhost:4200/`

## Heroku

https://young-bastion-11172.herokuapp.com

## ТЗ
При открытии приложения, должен отображаться:
+ Пустой дашборд  
![image](https://user-images.githubusercontent.com/18496223/62001999-0bc49780-b13f-11e9-995a-09455852756b.png)

+ Или ранее выбранные города  
![image](https://user-images.githubusercontent.com/18496223/62002007-3a427280-b13f-11e9-9b94-b06066a60abd.png)

В дашборде должна быть возможность:
+ Добавить новый город
+ При добавлении нового города на дашборд должно быть поле для поиска
+ Когда вы вводите туда какой-либо текст, должны отобразиться города, которые ему соответствуют  
![image](https://user-images.githubusercontent.com/18496223/62002024-81306800-b13f-11e9-8eb4-f54c7b2d0411.png)

+ Удалить город  
![image](https://user-images.githubusercontent.com/18496223/62002029-5510e900-b137-11e9-851f-d64f50035852.png)

+ Изменять расположение карточек городов с помощью drag-and-drop (в избранном)  
![image](https://user-images.githubusercontent.com/18496223/62002047-b20c9f00-b137-11e9-98d2-66653325446f.png)

Карточка города должна содержать следующую информацию:
+ Название города
+ Текущая погода
+ Дата  
![image](https://user-images.githubusercontent.com/18496223/62002080-a2de1f00-b140-11e9-8057-0bb367c65b63.png)

При клике на карточку с городом, должна быть показана страница:
+ С детальной информацией о погоде в этом городе на текущий день  
![image](https://user-images.githubusercontent.com/18496223/62002084-c1dcb100-b140-11e9-9465-68a3b4a8844a.png)

+ И краткая информация о погоде на ближайшие дни  
![image](https://user-images.githubusercontent.com/18496223/62002089-d91b9e80-b140-11e9-9bd6-c8cd7db8edbd.png)

Также реализуйте возможность добавлять город в раздел "Мои места":  
+ Как из дашборда  
![image](https://user-images.githubusercontent.com/18496223/62002112-23048480-b141-11e9-8d3d-2fcc156557ec.png)

+ Так и на странице с подробной информацией  
![image](https://user-images.githubusercontent.com/18496223/62002235-0cf7c380-b143-11e9-8ee4-036cc5d4c989.png)

+ Список таких городов сохраняйте локально (localStorage, например)  
![image](https://user-images.githubusercontent.com/18496223/62002121-4a5b5180-b141-11e9-9c90-8547129968e3.png)

Предусмотрите:
+ Просмотр раздела "Мои места" где-то в приложении  
![image](https://user-images.githubusercontent.com/18496223/62002127-64952f80-b141-11e9-9efa-698eebca7a66.png)

+ И удаление из него  
![image](https://user-images.githubusercontent.com/18496223/62002131-71198800-b141-11e9-9bb6-fa6e070d97f2.png)

Также предусмотрите отображение на карточке города и его странице информации о том, что этот город добавлен в раздел "Мои места":  
![image](https://user-images.githubusercontent.com/18496223/62002112-23048480-b141-11e9-8d3d-2fcc156557ec.png)  
![image](https://user-images.githubusercontent.com/18496223/62002131-71198800-b141-11e9-9bb6-fa6e070d97f2.png)

Также должна быть возможность:
+ Делиться ссылкой на страницу прогноза  
![image](https://user-images.githubusercontent.com/18496223/62002141-a625da80-b141-11e9-8fe3-be99af3b6488.png)

+ При копировании ссылки на город открывался соответствующий прогноз  
![image](https://user-images.githubusercontent.com/18496223/62002151-bfc72200-b141-11e9-87a6-171a293d59eb.png)

Необходимо предусмотреть адаптивность:
![weather](https://user-images.githubusercontent.com/18496223/62002200-2fd1aa00-b13a-11e9-8953-df5979f3a479.gif)
